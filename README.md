# Bundle Markdown Encoder

A maven plugin to base64-encode a README.md file the Bundle-Description MANIFEST header for a bundle.

The un-encoded header is shown on that App's page in the App Manager UI in [Integrated Genome Browser](https://bioviz.org.).

By default, a README.md file in the same directory as the POM.xml file is encoded. 

Specify a different file using the **markdownFile** parameter as in the example below. 

* * *



```pom
<plugin>
  <groupId>com.lorainelab</groupId>
  <artifactId>bundle-markdown-encoder</artifactId>
  <version>0.0.1</version> 
   <executions>
      <execution>
        <goals>
          <goal>encodeMarkdown</goal>
        </goals>
        <configuration>
            <markdownFile>TEST.md</markdownFile>
        </configuration>
      </execution> 
    </executions>
</plugin>   
```
